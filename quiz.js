let currentQuestion = 0;
let score = 0;
let sumQuestions = questions.length;

let container = document.getElementById('quizContainer');
let questionEl = document.getElementById('question');
let opt1= document.getElementById('option1');
let opt2= document.getElementById('option2');
let opt3= document.getElementById('option3');
let opt4= document.getElementById('option4');
let nextButton = document.getElementById('nextButton');
let resultCont = document.getElementById('result');

nextButton.addEventListener('click', loadNextQuestion);

function loadQuestion (questionIndex)  {
    let q = questions[questionIndex];
    questionEl.textContent = [questionIndex + 1] + '. ' + q.question;
    opt1.textContent = q.option1;
    opt2.textContent = q.option2;
    opt3.textContent = q.option3;
    opt4.textContent = q.option4;
}

loadQuestion(currentQuestion); 

function loadNextQuestion ()  {
    let selectedOption = document.querySelector('input[type=radio]:checked');
    if(!selectedOption)  {
        alert('Morate izabrati jedan od ponuđenih odgovora!');
    }
    let answer = selectedOption.value;
    if(questions[currentQuestion].answer == answer)  {
        score +=1;
    }
    selectedOption.checked = false;
    currentQuestion++;
    if(currentQuestion == sumQuestions - 1)  {
        nextButton.textContent = 'Rezultat';
    }
    if(currentQuestion == sumQuestions)  {
        container.style.display = 'none';
        resultCont.style.display = '';
        resultCont.textContent = 'Broj Vaših tačnih odgovora je ' + score + ' od mogućih ' + sumQuestions;
        return;
    }
    loadQuestion(currentQuestion);
}  
