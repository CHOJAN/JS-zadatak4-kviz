var questions = [ {
    "question" : "Koja je najduža reka na svetu?",
    "option1" : "Nil",
    "option2" : "Dunav",
    "option3" : "Amazon",
    "option4" : "Jegrička",
    "answer" : "3"
},  {
    "question" : "Koja je najmnogoljudnija država na svetu?",
    "option1" : "SAD",
    "option2" : "Indija",
    "option3" : "Kina",
    "option4" : "San Marino",
    "answer" : "3"
},  {
    "question" : "Koja od ovih država nije ostrvska?",
    "option1" : "Island",
    "option2" : "Malta",
    "option3" : "Velika Britanija",
    "option4" : "Srbija",
    "answer" : "4"
},  {
    "question" : "Koji je glavni grad Nigerije?",
    "option1" : "Abudža",
    "option2" : "Kigali",
    "option3" : "Džibuti",
    "option4" : "Najrobi",
    "answer" : "1"
},  {
    "question" : "Koji od ovih kontinenata je najveći po površini?",
    "option1" : "Severna Amerika",
    "option2" : "Evropa",
    "option3" : "Azija",
    "option4" : "Afrika",
    "answer" : "3"
},  {
    "question" : "Najduži kontinentalni planinski venac na svetu je?",
    "option1" : "Pamir(Himalaji)",
    "option2" : "Kordiljeri",
    "option3" : "Andi",
    "option4" : "Karakorum",
    "answer" : "2"
},  {
    "question" : "Najviše jezero na svetu je?",
    "option1" : "Bajkalsko",
    "option2" : "Titikaka",
    "option3" : "Tanganjika",
    "option4" : "Anđeosko",
    "answer" : "2"
}
]